#ifndef GNUSTEP
#include <GNUstepBase/GNUstep.h>
#endif

#import "SignUp.h"
#import "IKBCommandBus.h"
#import "AddPersonCommandHandler.h"

@implementation SignUp

- (id)init
{
  if ((self = [super init])) {
    [WOMessage setDefaultEncoding: NSUTF8StringEncoding];
    NSString *directActionHandlerKey = [[self class] directActionRequestHandlerKey];
    WORequestHandler *directActionHandler = [self requestHandlerForKey: directActionHandlerKey];
    [self setDefaultRequestHandler: directActionHandler];
    [self configureCommandHandlers];
  }
  return self;
}

- (void)configureCommandHandlers
{
  IKBCommandBus *bus = [IKBCommandBus applicationCommandBus];
  [bus registerCommandHandler: [[AddPersonCommandHandler new] autorelease]];
}

+ (NSNumber *)sessionTimeOut
{
  return [NSNumber numberWithInt:60];
}

@end
