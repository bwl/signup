// _Person.h
//
// Created by EOModelEditor.
// DO NOT EDIT. Make changes to Person.h instead.

#ifndef ___Person_h_
#define ___Person_h_

#import <EOControl/EOControl.h>


@interface _Person : EOCustomObject
{
  NSString *_email;
  NSString *_personName;
  NSDecimalNumber *_wouldPay;
}

- (void) setEmail:(NSString *) aValue;
- (NSString *) email;

- (void) setPersonName:(NSString *) aValue;
- (NSString *) personName;

- (void) setWouldPay:(NSDecimalNumber *) aValue;
- (NSDecimalNumber *) wouldPay;

@end

#endif //___Person_h_
