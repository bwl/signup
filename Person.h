// Person.h

#ifndef __Person_h_
#define __Person_h_

#import "_Person.h"

@interface Person: _Person
{
  // Custom instance variables go here
}

// Business logic methods go here

@end

#endif //__Person_h_
