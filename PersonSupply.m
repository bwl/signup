#import "PersonSupply.h"
#import "Person.h"

#import <EOControl/EOEditingContext.h>
#import <EOControl/EOClassDescription.h>
#import <EOControl/EOObjectStoreCoordinator.h>
#import <EOControl/EOFetchSpecification.h>
#import <EOControl/EOQualifier.h>

#import <EOAccess/EOModel.h>
#import <EOAccess/EODatabase.h>
#import <EOAccess/EODatabaseContext.h>
#import <EOAccess/EOEntity.h>

@implementation PersonSupply
{
  EOEditingContext *_editingContext;
}

static EOObjectStoreCoordinator *_databaseCoordinator = nil;
static EOEntity *_personEntity = nil;
+ (EOObjectStoreCoordinator *)databaseCoordinator
{
  @synchronized(self)
  {
    if (_databaseCoordinator) return _databaseCoordinator;
    EOModel *model = [EOModel modelWithContentsOfFile: [[NSBundle mainBundle] pathForResource: @"SignUp" ofType: @"eomodeld"]];
    EODatabase *database = [EODatabase databaseWithModel: model];
    EODatabaseContext *dbContext = [EODatabaseContext databaseContextWithDatabase: database];
    EOObjectStoreCoordinator *coordinator = [EOObjectStoreCoordinator new];
    [coordinator addCooperatingObjectStore: dbContext];
    _databaseCoordinator = coordinator;
    _personEntity = [[model entityNamed: @"Person"] retain];
    return _databaseCoordinator;
  }
}

+ (EOEntity *)personEntity
{
  [self databaseCoordinator];
  return _personEntity;
}

- (instancetype)init
{
  self = [super init];
  if (self)
  {
    _editingContext = [[EOEditingContext alloc] initWithParentObjectStore: [[self class] databaseCoordinator]];
  }
  return self;
}

- (void)dealloc
{
  [_editingContext release];
  [super dealloc];
}

- (EOEditingContext *)editingContext
{
  return _editingContext;
}

- (Person *)insertedPerson
{
  Person *newPerson = nil;
  newPerson = [[[[self class] personEntity] classDescriptionForInstances]
                     createInstanceWithEditingContext: nil
                                             globalID: nil
                                                 zone: NULL];
  [[self editingContext] insertObject: newPerson];
  return [[newPerson retain] autorelease];
}

- (Person *)personWithEmail: (NSString *)emailAddress
{
  if (!emailAddress) return [self insertedPerson];
  EOQualifier *qualifier = [EOQualifier qualifierWithQualifierFormat: @"email like %@", emailAddress];
  EOFetchSpecification *fetchSpec = [EOFetchSpecification fetchSpecificationWithEntityName: @"Person"
                                                                                 qualifier: qualifier
                                                                             sortOrderings: nil];
  NSArray *fetchedPeople = [[self editingContext] objectsWithFetchSpecification: fetchSpec];
  if ([fetchedPeople count] > 0) return [[[fetchedPeople lastObject] retain] autorelease];
  else
  {
    Person *insertedPerson = [self insertedPerson];
    insertedPerson.email = emailAddress;
    return insertedPerson;
  }
}

- (void)saveChanges
{
  [[self editingContext] saveChanges];
}

@end