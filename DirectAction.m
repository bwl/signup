#ifndef GNUSTEP
#include <GNUstepBase/GNUstep.h>
#endif

#import "DirectAction.h"
#import "SignUpForm.h"

@implementation DirectAction

- (GSWComponent *)defaultAction
{
  return [self pageWithName: @"Main"];
}

- (GSWComponent *)registerInterestAction
{
  GSWRequest *request = [self request];
  [SignUpForm processSignUp: request];
  return [self pageWithName: @"Thanks"];
}


@end
