#include <WebObjects/WebObjects.h>

@interface SignUpForm:GSWComponent

+ (void)processSignUp: (GSWRequest *)request;

@end