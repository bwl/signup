#import <Foundation/Foundation.h>
#import "IKBCommand.h"

@interface AddPersonCommand : NSObject <IKBCommand>

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *email;
@property (nonatomic, readonly) BOOL shouldContact;
@property (nonatomic, readonly) NSDecimalNumber *willingToPay;

+ (instancetype)commandToAddPersonNamed: (NSString *)name email: (NSString *)email canBeContacted: (BOOL)shouldContact willingToPay: (NSDecimalNumber *)dollarAmount;

@end
