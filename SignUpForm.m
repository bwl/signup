#ifndef GNUSTEP
#include <GNUstepBase/GNUstep.h>
#endif

#import "SignUpForm.h"
#import "AddPersonCommand.h"
#import "IKBCommandBus.h"

@interface SignUpForm ()

@property (nonatomic, strong) NSString *personName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) BOOL shouldContact;
@property (nonatomic, strong) NSString *cost;

@end

@implementation SignUpForm

+ (void)processSignUp: (GSWRequest *)request
{
  NSDecimalNumber *dollarAmount = nil;
  NSString *cost = [request formValueForKey: @"cost"];
  if (cost)
    dollarAmount = [NSDecimalNumber decimalNumberWithString: cost];
  AddPersonCommand *command = [AddPersonCommand commandToAddPersonNamed: [request formValueForKey: @"personName"] email: [request formValueForKey: @"email"] canBeContacted: [[request formValueForKey: @"shouldContact"] boolValue] willingToPay: dollarAmount];
  [[IKBCommandBus applicationCommandBus] execute: command];
}

@end
